BTN_LEFT    = ?1
BTN_MIDDLE  = ?2
BTN_RIGHT   = ?3
SCR_UP      = ?4
SCR_DOWN    = ?5
SCR_LEFT    = ?6
SCR_RIGHT   = ?7
BTN_BACK    = ?8
BTN_FORWARD = ?9
NO_BTN      = ""

def btn_code
  ENV['BLOCK_BUTTON']
end

def btn
  case btn_code
  when BTN_LEFT
    :btn_left
  when BTN_MIDDLE
    :btn_middle
  when BTN_RIGHT
    :btn_right
  when SCR_UP
    :scr_up
  when SCR_DOWN
    :scr_down
  when SCR_LEFT
    :scr_left
  when SCR_RIGHT
    :scr_right
  when BTN_BACK
    :btn_back
  when BTN_FORWARD
    :btn_forward
  when NO_BTN
    :no_btn
  else
    :none
  end
end

def notify_send(title, msg=nil)
  command = [
             "notify-send",
             "#{title}",
             ("#{msg}" if msg)
            ].join(" ")
  `#{command}`
end

def run(path)
  `i3-msg exec #{path}`
end

%i[btn_left btn_middle btn_right scr_up scr_down scr_left scr_right btn_back btn_forward no_btn].each do |btn_name|
  define_method(btn_name) do |&block|
    block.call if btn_name == btn
  end
end

def on_mouse_event
  yield if btn != :no_btn
end

def on_click
  yield if %i[btn_left btn_middle btn_right].include?(btn)
end

def on_scroll
  yield if %i[scr_up scr_down scr_left scr_right].include?(btn)
end

def output(str=nil)
  @output ||= ""
  @output << str.to_s if str
  @output
end
